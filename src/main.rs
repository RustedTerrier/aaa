// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
mod terminal;
use std::{env, path::Path, fs, process::exit};

use terminal::Terminal;
use xekeys::*;

fn main() {
    let args: Vec<String> = env::args().collect();
    let path = &args[1];
    match run_app(path) {
        | Ok(_) => (),
        | Err(e) => {
            eprintln!("{}", e);
            exit(1);
        }
    }
}

fn run_app(path: &str) -> Result<(), String> {
    if path.is_empty() {
        Err("No file path given: aborting.".to_string())
    } else {
        let term = Terminal::default();
        let mut list = read_to_list(path);
        let mut selection = 0;
        loop {
            if selection >= get_full_len(&list) - 1 {
                selection = get_full_len(&list) - 1;
            }
            let mut size = term.get_size();
            match get_keys() {
                | Keys::CtrlChar('q') => {
                    break;
                },
                | Keys::CtrlChar('w') => {
                    let mut prompt = path.to_string();
                    loop {
                        size = term.get_size();
                        match get_keys() {
                            | Keys::Char('\n') => break,
                            | Keys::Char(a) => prompt.push(a),
                            | Keys::Backspace => {
                                prompt.pop();
                                ()
                            },
                            | _ => ()
                        }
                        render_bar(size, Some(&prompt), &list);
                    }
                    if prompt.ends_with(".md") {
                        write(&prompt, &list, Extract::Markdown);
                    }else{
                        write(&prompt, &list, Extract::Xedoc);
                    }
                },
                | Keys::Char('a') => {
                    let mut prompt = String::new();
                    loop {
                        size = term.get_size();
                        match get_keys() {
                            | Keys::Char('\n') => break,
                            | Keys::Char(a) => prompt.push(a),
                            | Keys::Backspace => {
                                prompt.pop();
                                ()
                            },
                            | _ => ()
                        }
                        render_bar(size, Some(&prompt), &list);
                    }
                    list.push(Item { done: false, sub_items: Vec::new(), name: prompt });
                },
                | Keys::Char('A') => {
                    list = del_sub_item(selection, list, 0);
                },
                | Keys::CtrlChar('a') => {
                    let mut prompt = String::new();
                    loop {
                        size = term.get_size();
                        match get_keys() {
                            | Keys::Char('\n') => break,
                            | Keys::Char(a) => prompt.push(a),
                            | Keys::Backspace => {
                                prompt.pop();
                                ()
                            },
                            | _ => ()
                        }
                        render_bar(size, Some(&prompt), &list);
                    }
                    list = add_sub_item(selection, list, prompt, 0);
                }
                | Keys::Char(' ') => {
                    list = check_sub_item(selection, list, 0, false);
                },
                | Keys::Char('\n') => {
                    list = check_sub_item(selection, list, 0, true);
                },
                | Keys::Arrow(ArrowKey::Up) => {
                    if selection != 0 {
                        selection -= 1;
                    }
                },
                | Keys::Arrow(ArrowKey::Down) => {
                    if selection < get_full_len(&list) - 1 {
                        selection += 1;
                    }
                },
                | _ => ()
            }
            render_window(&list, selection, &path);
            render_bar(size, None, &list);
        }
        Ok(())
    }
}

fn render_bar(size: (u16, u16), message: Option<&str>, list: &[Item]) {
    let done = get_done_items(list);
    let undone = get_full_len(list) - done;
    match message {
        | Some(a) => println!("\x1b[{};0H{}{}\x1b[H", size.1 - 2, a, " ".repeat(size.0 as usize - a.len())),
        | None => {
                let bar = format!("{}{}/{}", " ".repeat(size.0 as usize - (done.to_string().len() + 1 + (done + undone).to_string().len())), done, undone + done);
                println!("\x1b[{};0H\x1b[7m{}\x1b[0m\x1b[H", size.1 - 2, bar);
        }
    }
}

fn render_window(list: &[Item], selection: usize, path: &str) {
    println!("\x1b[2J");
    println!("\x1b[H{}", path);
    read_items(list, selection, 0, 0);
}

fn read_items(list: &[Item], selection: usize, indent: usize, mut num_passed: usize) -> usize {
    for (j, i) in list.iter().enumerate() {
        match j + num_passed == selection {
            | true => match i.done {
                | true => println!("\r{}\x1b[7m[X] {}\x1b[0m", "\t".repeat(indent), i.name),
                | false => println!("\r{}\x1b[7m[ ] {}\x1b[0m", "\t".repeat(indent), i.name)
            },
            | false => match i.done {
                | true => println!("\r{}[X] {}", "\t".repeat(indent), i.name),
                | false => println!("\r{}[ ] {}", "\t".repeat(indent), i.name)
            }
        }
        if !i.sub_items.is_empty() {
            num_passed = read_items(&i.sub_items, selection, indent + 1, j + num_passed + 1) + i.sub_items.len() - j - 1;
        }
    }
    num_passed
}

#[derive(Clone)]
struct Item {
    done:      bool,
    sub_items: Vec<Item>,
    name:      String
}

fn get_full_len(items: &[Item]) -> usize {
    let mut num = 0;
    for i in items {
        num += 1;
        num += get_full_len(&i.sub_items);
    }
    num
}

fn get_done_items(items: &[Item]) -> usize {
    let mut num = 0;
    for i in items {
        if i.done {
            num += 1;
        }
        num += get_done_items(&i.sub_items);
    }
    num
}

fn add_sub_item(selection: usize, mut list: Vec<Item>, name: String, mut counter: usize) -> Vec<Item> {
    for i in counter .. list.len() {
        if counter == selection {
            list[i].sub_items.push(Item { done: false, sub_items: Vec::new(), name });
            break;
        }else{
            if !list[i].sub_items.is_empty() {
                counter += list[i].sub_items.len();
            }
            counter += 1;
        }
    }
    list.clone()
}

fn del_sub_item(selection: usize, mut list: Vec<Item>, mut counter: usize) -> Vec<Item> {
    for i in counter .. list.len() {
        if counter == selection {
            list.remove(i);
            break;
        }else{
            if !list[i].sub_items.is_empty() {
                for j in 0 .. list[i].sub_items.len() {
                    counter += 1;
                    if counter == selection {
                        list[i].sub_items.remove(j);
                        break;
                    }
                }
                if counter == selection {
                    break;
                }
            }
            counter += 1;
        }
    }
    list.clone()
}

fn check_sub_item(selection: usize, mut list: Vec<Item>, mut counter: usize, all: bool) -> Vec<Item> {
    for i in counter .. list.len() {
        if counter == selection {
            list[i].done = !list[i].done;
            if all {
                for j in 0 .. list[i].sub_items.len() {
                    list[i].sub_items[j].done = list[i].done;
                }
            }
            break;
        }else{
            if !list[i].sub_items.is_empty() {
                for j in 0 .. list[i].sub_items.len() {
                    counter += 1;
                    if counter == selection {
                        list[i].sub_items[j].done = !list[i].sub_items[j].done;
                        break;
                    }
                }
                if counter == selection {
                    break;
                }
            }
            counter += 1;
        }
    }
    list.clone()

}

enum Extract {
    Xedoc,
    Markdown
}

fn write(path: &str, list: &[Item], extract: Extract) {
    let mut file = String::new();
    match extract {
        | Extract::Xedoc => {
            for i in list {
                match i.done {
                    | true => file = format!("{}y {}", file, i.name),
                    | false => file = format!("{}n {}", file, i.name)
                };
                if !i.sub_items.is_empty() {
                    file = format!("{}\n^\n", file);
                    for j in &i.sub_items {
                        match j.done {
                            | true => file = format!("{}y {}", file, j.name),
                            | false => file = format!("{}n {}", file, j.name)
                        };
                        file = format!("{}\n", file);
                    }
                    file = format!("{}\nv\n", file);
                }
                file = format!("{}\n", file);
            }
            let file = file.trim_end_matches("\n");

            match fs::write(path, file) {
                | Ok(a) => a,
                | Err(e) => {
                    eprintln!("Unable to write to {}: {}", path, e);
                    exit(1);
                }
            }
        },
        | Extract::Markdown => {
            for i in list {
                match i.done {
                    | true => file = format!("{} - [X] {}", file, i.name),
                    | false => file = format!("{} - [ ] {}", file, i.name)
                };
                if !i.sub_items.is_empty() {
                    file = format!("{}\n", file);
                    for j in &i.sub_items {
                        match j.done {
                            | true => file = format!("{}   - [X] {}", file, j.name),
                            | false => file = format!("{}   - [ ] {}", file, j.name)
                        };
                        file = format!("{}\n", file);
                    }
                    file = format!("{}\n", file);
                }
                file = format!("{}\n", file);
            }
            let file = file.trim_end_matches("\n");

            match fs::write(path, file) {
                | Ok(a) => a,
                | Err(e) => {
                    eprintln!("Unable to write to {}: {}", path, e);
                    exit(1);
                }
            }
        }
    }
}

fn read_to_list(path: &str) -> Vec<Item> {
    if Path::new(&path).exists() {
        let file = match fs::read_to_string(&path) {
            | Ok(a) => a,
            | Err(e) => {
                eprintln!("Unable to read file {}: {}", path, e);
                exit(1);
            }
        };

        let mut list: Vec<Item> = Vec::new();
        let file_lines: Vec<&str> = file.split('\n').collect();
        let mut i = 0;
        while i < file_lines.len() {
            if file_lines[i].starts_with("y ") {
                list.push(Item {done: true, sub_items: Vec::new(), name: file_lines[i].trim_start_matches("y ").to_string()});
                i += 1;
            }else if file_lines[i].starts_with("n ") {
                list.push(Item {done: false, sub_items: Vec::new(), name: file_lines[i].trim_start_matches("n ").to_string()});
                i += 1;
            }else if file_lines[i] == "^" {
                let list_last_value = list.len() - 1;
                while file_lines[i] != "v" && i < file_lines.len() {
                    let mut list_item = Item {
                        done: false, 
                        sub_items: Vec::new(),
                        name: String::new()
                    };
                    if file_lines[i].starts_with("y ") {
                        list_item = Item {done: true, sub_items: Vec::new(), name: file_lines[i].trim_start_matches("y ").to_string()};
                    }else if file_lines[i].starts_with("n ") {
                        list_item = Item {done: false, sub_items: Vec::new(), name: file_lines[i].trim_start_matches("n ").to_string()};
                    }
                    if !list_item.name.is_empty() {
                        list[list_last_value].sub_items.push(list_item)
                    }
                    i += 1;
                }
            }else{
                i += 1;
            }
        }
        list
    }else{
        Vec::new()
    }
}
