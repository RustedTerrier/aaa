# AAA: Awesome Agile Agenda
AAA aims to be a terminal based, fast, minimal, and customizable agenda/todo-list, in that order.

## Installing
AAA should work on all UNIX and UNIX-like systems. To compile run `cargo run --release` and then `cp target/release/aaa /usr/bin` to copy the binary to the path.

## Usage
aaa FILENAME

## Keymappings
 - a
    - Add an item
 - A
    - Remove an item
 - Ctrl-a
    - Add a sub-item(only works on non-sub-items for now)
 - Ctrl-q
    - Quit
 - Ctrl-w
    - Write to file(changing the file extension to .md exports as markdown instead of xedoc)
 - Space
    - Mark item as done
 - Enter
    - Mark item and sub-items as done

## Dependencies
 - Xekeys(written by me)
 - Libc

## License
MPL-2.0
